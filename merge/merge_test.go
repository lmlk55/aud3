package merge_test

import (
	"fmt"
	"math"
	"math/rand"
	"testing"
	"time"

	"git.thm.de/lmlk55/aud3/merge"
)

func init() {
	rand.Seed(time.Now().Unix())
}

func ExampleSort() {
	input := []int{5, 4, 3, 2, 1}
	sorted := merge.Sort(input)
	fmt.Println(sorted)
	// Output: [1 2 3 4 5]
}

func TestSort(t *testing.T) {
	for k := 1; k <= 16; k++ {
		t.Run(fmt.Sprintf("2^%d", k), testSort(k))
		t.Log("Runtime:", merge.GetCount())
	}
}

func testSort(size int) func(*testing.T) {
	input := rand.Perm(calcSize(size))
	return func(t *testing.T) {
		_ = merge.Sort(input)
	}
}

func BenchmarkSort(b *testing.B) {
	for k := 1; k <= 16; k++ {
		b.Run(fmt.Sprintf("2^%d", k), benchmarkSort(k))
	}
}

func benchmarkSort(size int) func(*testing.B) {
	input := rand.Perm(calcSize(size))
	return func(b *testing.B) {
		for n := 0; n < b.N; n++ {
			_ = merge.Sort(input)
		}
	}
}

func calcSize(size int) int {
	return int(math.Pow(float64(2), float64(size)))
}
