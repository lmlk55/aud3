package merge

var count int

// GetCount returns the counter value and resets it.
func GetCount() int {
	c := count
	count = 0
	return c
}

// Sort implements the merge sort algorithm.
func Sort(input []int) []int {
	if len(input) <= 1 {
		return input
	}
	m := len(input) / 2
	left := Sort(input[:m])
	right := Sort(input[m:])
	return merge(left, right)
}

// merge merges both slices a and b into one slice.
func merge(left, right []int) []int {
	count += len(left) + len(right)
	res := make([]int, len(left)+len(right))
	var i, j, k int
	for i < len(left) {
		for j < len(right) && right[j] <= left[i] {
			res[k] = right[j]
			k++
			j++
		}
		res[k] = left[i]
		k++
		i++
	}
	for j < len(right) {
		res[k] = right[j]
		k++
		j++
	}
	return res
}
