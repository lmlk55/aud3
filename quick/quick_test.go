package quick_test

import (
	"fmt"
	"math"
	"math/rand"
	"testing"
	"time"

	"git.thm.de/lmlk55/aud3/quick"
)

func init() {
	rand.Seed(time.Now().Unix())
}

func ExampleSort() {
	input := []int{5, 4, 3, 2, 1}
	sorted := quick.Sort(input)
	fmt.Println(sorted)
	// Output: [1 2 3 4 5]
}

func TestSort(t *testing.T) {
	for k := 1; k <= 16; k++ {
		for i := 0; i < 20; i++ {
			t.Run(fmt.Sprintf("2^%d_Random", k), testSortRandomCase(k))
		}
		t.Log("Runtime:", quick.GetCount()/20)
	}
	for k := 1; k <= 16; k++ {
		t.Run(fmt.Sprintf("2^%d_Worst", k), testSortWorstCase(k))
		t.Log("Runtime:", quick.GetCount())
	}
}

func testSortRandomCase(size int) func(*testing.T) {
	input := rand.Perm(calcSize(size))
	return testSort(input)
}

func testSortWorstCase(size int) func(*testing.T) {
	size = calcSize(size)
	input := make([]int, size)
	for i := 0; i < size; i++ {
		input[i] = i + 1
	}
	return testSort(input)
}

func testSort(input []int) func(*testing.T) {
	return func(t *testing.T) {
		_ = quick.Sort(input)
	}
}

func BenchmarkSort(b *testing.B) {
	for k := 1; k <= 16; k++ {
		b.Run(fmt.Sprintf("2^%d_Random", k), benchmarkSortRandomCase(k))
		b.Run(fmt.Sprintf("2^%d_Worst", k), benchmarkSortWorstCase(k))
	}
}

func benchmarkSortRandomCase(size int) func(*testing.B) {
	return benchmarkSort(rand.Perm(calcSize(size)))
}

func benchmarkSortWorstCase(size int) func(*testing.B) {
	size = calcSize(size)
	input := make([]int, size)
	for i := 0; i < size; i++ {
		input[i] = i + 1
	}
	return benchmarkSort(input)
}

func benchmarkSort(input []int) func(*testing.B) {
	return func(b *testing.B) {
		for n := 0; n < b.N; n++ {
			_ = quick.Sort(input)
		}
	}
}

func calcSize(size int) int {
	return int(math.Pow(float64(2), float64(size)))
}
