package quick

var count int

// GetCount returns the counter value and resets it.
func GetCount() int {
	c := count
	count = 0
	return c
}

// Sort implements the quick sort algorithm.
func Sort(input []int) []int {
	if len(input) <= 1 {
		return input
	}
	m := partition(input)
	_ = Sort(input[:m])
	_ = Sort(input[m:])
	return input
}

// partition partitions the input slice and returns the pivot index.
func partition(input []int) int {
	count += len(input)
	max := len(input) - 1
	p := input[max]
	var m int
	for i := 0; i <= max; i++ {
		if input[i] < p {
			input[i], input[m] = input[m], input[i]
			m++
		}
	}
	input[m], input[max] = input[max], input[m]
	return m
}
