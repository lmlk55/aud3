module git.thm.de/lmlk55/aud3

require (
	github.com/CodingBerg/benchgraph v0.0.0-20160127080251-f9c026b66777
	github.com/fatih/color v1.7.0 // indirect
	github.com/mattn/go-colorable v0.0.9 // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	golang.org/x/perf v0.0.0-20180704124530-6e6d33e29852
	golang.org/x/sys v0.0.0-20181031143558-9b800f95dbbc // indirect
	golang.org/x/tools v0.0.0-20181101071927-45ff765b4815 // indirect
)
