# Algorithmen und Datenstrukturen - Übungsblatt 3

> MergeSort vs QuickSort

---

## Table of Contents

1. [Introduction](#introduction)
1. [Tests and Benchmarks](#tests-and-benchmarks)
1. [Benchmark Results](#benchmark-results)
1. [License](#license)

## Introduction

Package `aud3` implements the MergeSort and QuickSort sorting algorithms.
Benchmarks are included as well.

### Installation

A working go toolchain installation with at least Go 1.11 is required.

#### Install from source

```shell
git clone https://git.thm.de/lmlk55/aud3.git
cd aud3
```

## Tests and Benchmarks

### Preparation

The following tools are optional and can be installed to compare and visualize
benchmark results.

```shell
go install golang.org/x/perf/cmd/benchstat
```

### Tests

Run the tests by issuing the `go test ./...` command. You should see an output
similar to this:

```shell
?       git.thm.de/lmlk55/aud3  [no test files]
ok      git.thm.de/lmlk55/aud3/merge    0.035s
ok      git.thm.de/lmlk55/aud3/quick    3.023s
```

Add the `-v` flag to the previous command to print the full output which also
includes the counter results. The output is very large an thus omitted here. A
table with the results can be foudn below.

### Benchmarks

Run the benchmark by issuing the `go test -run=NONE -bench=. ./...` command. You
should see an output similar to this, depending on your operating system and
architecture:

```shell
?       git.thm.de/lmlk55/aud3  [no test files]
goos: darwin
goarch: amd64
pkg: git.thm.de/lmlk55/aud3/merge
BenchmarkSort/2^1-4             30000000            45.0 ns/op
BenchmarkSort/2^2-4             10000000           124 ns/op
BenchmarkSort/2^3-4              5000000           298 ns/op
BenchmarkSort/2^4-4              2000000           680 ns/op
BenchmarkSort/2^5-4              1000000          1505 ns/op
BenchmarkSort/2^6-4               500000          3233 ns/op
BenchmarkSort/2^7-4               200000          7121 ns/op
BenchmarkSort/2^8-4               100000         17363 ns/op
BenchmarkSort/2^9-4                30000         43019 ns/op
BenchmarkSort/2^10-4               10000        108536 ns/op
BenchmarkSort/2^11-4               10000        213507 ns/op
BenchmarkSort/2^12-4                3000        562547 ns/op
BenchmarkSort/2^13-4                2000       1014280 ns/op
BenchmarkSort/2^14-4                1000       2137714 ns/op
BenchmarkSort/2^15-4                 300       4641103 ns/op
BenchmarkSort/2^16-4                 100      15370126 ns/op
PASS
ok      git.thm.de/lmlk55/aud3/merge    27.886s
goos: darwin
goarch: amd64
pkg: git.thm.de/lmlk55/aud3/quick
BenchmarkSort/2^1_Random-4          50000000            36.5 ns/op
BenchmarkSort/2^1_Worst-4           30000000            48.4 ns/op
BenchmarkSort/2^2_Random-4          30000000            65.9 ns/op
BenchmarkSort/2^2_Worst-4           30000000            55.2 ns/op
BenchmarkSort/2^3_Random-4          10000000           130 ns/op
BenchmarkSort/2^3_Worst-4           20000000           110 ns/op
BenchmarkSort/2^4_Random-4           5000000           306 ns/op
BenchmarkSort/2^4_Worst-4            5000000           296 ns/op
BenchmarkSort/2^5_Random-4           1000000          1100 ns/op
BenchmarkSort/2^5_Worst-4            1000000          1011 ns/op
BenchmarkSort/2^6_Random-4            500000          3440 ns/op
BenchmarkSort/2^6_Worst-4             500000          3489 ns/op
BenchmarkSort/2^7_Random-4            200000         11104 ns/op
BenchmarkSort/2^7_Worst-4             200000         11086 ns/op
BenchmarkSort/2^8_Random-4             50000         37178 ns/op
BenchmarkSort/2^8_Worst-4              50000         37555 ns/op
BenchmarkSort/2^9_Random-4             10000        133270 ns/op
BenchmarkSort/2^9_Worst-4              10000        133548 ns/op
BenchmarkSort/2^10_Random-4             3000        516080 ns/op
BenchmarkSort/2^10_Worst-4              3000        504165 ns/op
BenchmarkSort/2^11_Random-4             1000       1950380 ns/op
BenchmarkSort/2^11_Worst-4              1000       2008821 ns/op
BenchmarkSort/2^12_Random-4              200       8205219 ns/op
BenchmarkSort/2^12_Worst-4               200       7577961 ns/op
BenchmarkSort/2^13_Random-4              100      29740563 ns/op
BenchmarkSort/2^13_Worst-4                50      30014642 ns/op
BenchmarkSort/2^14_Random-4              100     138445473 ns/op
BenchmarkSort/2^14_Worst-4                10     233984766 ns/op
BenchmarkSort/2^15_Random-4              100     498315314 ns/op
BenchmarkSort/2^15_Worst-4                 3     477728050 ns/op
BenchmarkSort/2^16_Random-4              100    2030285668 ns/op
BenchmarkSort/2^16_Worst-4                 1    2024710801 ns/op
PASS
ok      git.thm.de/lmlk55/aud3/quick    323.865s
```

## Benchmark Results

**Device**: MacBook Pro, i5 2.7 GhZ, 8GB

The following benchmarks have been created using this sequence of commands:

```shell
go test -run=NONE -bench=. -benchmem ./merge | tee -a merge-benchmark
go test -run=NONE -bench=. -benchmem ./quick | tee -a quick-benchmark
benchstat merge-benchmark quick-benchmark
```

### MergeSort

| Input Size 2<sup>k</sup> | time/op     | alloc/op    | allocs/op  |
|--------------------------|-------------|-------------|------------|
| 2<sup>1</sup>            | 45.6ns ± 0% | 16.0B ± 0%  | 1.00 ± 0%  |
| 2<sup>2</sup>            | 122ns ± 0%  | 64.0B ± 0%  | 3.00 ± 0%  |
| 2<sup>3</sup>            | 304ns ± 0%  | 192B ± 0%   | 7.00 ± 0%  |
| 2<sup>4</sup>            | 673ns ± 0%  | 512B ± 0%   | 15.00 ± 0% |
| 2<sup>5</sup>            | 1.52µs ± 0% | 1.28kB ± 0% | 31.00 ± 0% |
| 2<sup>6</sup>            | 3.34µs ± 0% | 3.07kB ± 0% | 63.00 ± 0% |
| 2<sup>7</sup>            | 7.07µs ± 0% | 7.17kB ± 0% | 127 ± 0%   |
| 2<sup>8</sup>            | 17.3µs ± 0% | 16.4kB ± 0% | 255 ± 0%   |
| 2<sup>9</sup>            | 42.5µs ± 0% | 36.9kB ± 0% | 511 ± 0%   |
| 2<sup>10</sup>           | 97.1µs ± 0% | 81.9kB ± 0% | 1.02k ± 0% |
| 2<sup>11</sup>           | 212µs ± 0%  | 180kB ± 0%  | 2.05k ± 0% |
| 2<sup>12</sup>           | 453µs ± 0%  | 393kB ± 0%  | 4.09k ± 0% |
| 2<sup>13</sup>           | 972µs ± 0%  | 852kB ± 0%  | 8.19k ± 0% |
| 2<sup>14</sup>           | 2.03ms ± 0% | 1.84MB ± 0% | 16.4k ± 0% |
| 2<sup>15</sup>           | 4.32ms ± 0% | 3.93MB ± 0% | 32.8k ± 0% |
| 2<sup>16</sup>           | 13.4ms ± 0% | 8.39MB ± 0% | 65.5k ± 0% |

### QuickSort

#### Random (Average) Case

| Input Size 2<sup>k</sup> | time/op     | alloc/op | allocs/op |
|--------------------------|-------------|----------|-----------|
| 2<sup>1</sup>            | 21.5ns ± 0% | 0.00B    | 0.00      |
| 2<sup>2</sup>            | 46.9ns ± 0% | 0.00B    | 0.00      |
| 2<sup>3</sup>            | 134ns ± 0%  | 0.00B    | 0.00      |
| 2<sup>4</sup>            | 304ns ± 0%  | 0.00B    | 0.00      |
| 2<sup>5</sup>            | 1.08µs ± 0% | 0.00B    | 0.00      |
| 2<sup>6</sup>            | 3.58µs ± 0% | 0.00B    | 0.00      |
| 2<sup>7</sup>            | 11.3µs ± 0% | 0.00B    | 0.00      |
| 2<sup>8</sup>            | 44.8µs ± 0% | 0.00B    | 0.00      |
| 2<sup>9</sup>            | 139µs ± 0%  | 0.00B    | 0.00      |
| 2<sup>10</sup>           | 522µs ± 0%  | 0.00B    | 0.00      |
| 2<sup>11</sup>           | 1.98ms ± 0% | 0.00B    | 0.00      |
| 2<sup>12</sup>           | 8.10ms ± 0% | 0.00B    | 0.00      |
| 2<sup>13</sup>           | 31.8ms ± 0% | 0.00B    | 0.00      |
| 2<sup>14</sup>           | 157ms ± 0%  | 0.00B    | 0.00      |
| 2<sup>15</sup>           | 608ms ± 0%  | 0.00B    | 0.00      |
| 2<sup>16</sup>           | 2.32s ± 0%  | 0.00B    | 0.00      |

#### Worst Case

| Input Size 2<sup>k</sup> | time/op     | alloc/op | allocs/op |
|--------------------------|-------------|----------|-----------|
| 2<sup>1</sup>            | 18.0ns ± 0% | 0.00B    | 0.00      |
| 2<sup>2</sup>            | 51.7ns ± 0% | 0.00B    | 0.00      |
| 2<sup>3</sup>            | 121ns ± 0%  | 0.00B    | 0.00      |
| 2<sup>4</sup>            | 320ns ± 0%  | 0.00B    | 0.00      |
| 2<sup>5</sup>            | 1.03µs ± 0% | 0.00B    | 0.00      |
| 2<sup>6</sup>            | 3.53µs ± 0% | 0.00B    | 0.00      |
| 2<sup>7</sup>            | 12.0µs ± 0% | 0.00B    | 0.00      |
| 2<sup>8</sup>            | 39.2µs ± 0% | 0.00B    | 0.00      |
| 2<sup>9</sup>            | 143µs ± 0%  | 0.00B    | 0.00      |
| 2<sup>10</sup>           | 518µs ± 0%  | 0.00B    | 0.00      |
| 2<sup>11</sup>           | 1.95ms ± 0% | 0.00B    | 0.00      |
| 2<sup>12</sup>           | 7.75ms ± 0% | 0.00B    | 0.00      |
| 2<sup>13</sup>           | 31.7ms ± 0% | 0.00B    | 0.00      |
| 2<sup>14</sup>           | 131ms ± 0%  | 0.00B    | 0.00      |
| 2<sup>15</sup>           | 514ms ± 0%  | 0.00B    | 0.00      |
| 2<sup>16</sup>           | 2.52s ± 0%  | 0.00B    | 0.00      |

### MergeSort vs QuickSort

![merge_quick][merge_quick]

![merge_quick_graph][merge_quick_graph]

## License

© Lukas Malkmus, 2018

Distributed under MIT License (`The MIT License`).

See [LICENSE](LICENSE) for more information.

<!-- Links -->
[merge_quick]: .gitlab/merge_quick.png "MergeSort vs QuickSort Runtime in ns"
[merge_quick_graph]: .gitlab/merge_quick_graph.png "MergeSort vs QuickSort Runtime in ns"